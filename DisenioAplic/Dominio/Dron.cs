﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio
{
    public class Dron
    {
        private int Id;
        private String Color;
        private String ModeloCR;
        private float Precio;

        public Dron(int Id, String Color, String ModeloCR, float Precio) {

            this.Color = Color;
            this.Id = Id;
            this.ModeloCR = ModeloCR;
            this.Precio = Precio;

        }

        public Dron()
        {
            // TODO: Complete member initialization
        }


        public String color
        {
            get { return Color; }
            set { Color = value; }
        }

        public String modeloCR
        {
            get { return ModeloCR; }
            set { ModeloCR = value; }
        }

        public int id
        {
            get { return Id; }
            set { Id = value; }
        }

        public float precio
        {
            get { return Precio; }
            set { Precio = value; }
        }

        public override string ToString()
        {
            return id + " - " + color + " - " + modeloCR + " - $" + precio;
        }
    }
}
