﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio
{
    public class Pieza
    {
        private String Marca;
        private String Modelo;
        private int Precision;
        private int Max;
        private int Min;

        public Pieza(String Marca, String Modelo, int Precision, int Max, int Min)
        {
            this.Marca = Marca;
            this.Modelo = Modelo;
            this.Precision = Precision;
            this.Max = Max;
            this.Min = Min;
        }

        public String marca
        {
            get { return Marca; }
            set { Marca = value; }
        }

        public String modelo
        {
            get { return Modelo; }
            set { Modelo = value; }
        }

        public int precision
        {
            get { return Precision; }
            set { Precision = value; }
        }

        public int max
        {
            get { return Max; }
            set { Max = value; }
        }

        public int min
        {
            get { return Min; }
            set { Min = value; }
        }

        public override string ToString()
        {
            return marca + " - " + modelo + " - " + precision + " - Max: " + max + " - Min: " + min;
        }
    }
}
