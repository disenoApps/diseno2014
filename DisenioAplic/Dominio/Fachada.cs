﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio
{
    public class Fachada
    {
        private Sistema sistema;

        #region SINGLETON
        private static Fachada fachada = null;

        private Fachada()
        {
            sistema = Sistema.getSistema();
        }

        public static Fachada getFachada()
        {
            if (fachada == null)
            {
                fachada = new Fachada();
            }
            return fachada;
        }
        #endregion

        #region PIEZA
        public void cearPieza(String marca, String modelo, int precision, int max, int min)
        {
            if (sistema.existePieza(marca, modelo) == false)
            {
                sistema.cearPieza(marca, modelo, precision, max, min);
            }
            else
            {
                throw new MiExcepcion("La pieza ya existe.");
            }
        }

        public List<Pieza> getListaPiezas()
        {
            return sistema.listaPiezas;
        }
        #endregion

        #region DRON
        public void cearDron(int Id, String color, String modeloCR, float precio)
        {
            if (sistema.existeDron(Id) == false)
            {
                sistema.cearDron(Id, color, modeloCR, precio);
            }
            else
            {
                throw new MiExcepcion("El dron ya existe.");
            }
        }

        public List<Dron> getListaDrones()
        {
            return sistema.listaDrones;
        }
        #endregion

        #region DISENIODRON
        public void cearDisenioDron(String nombre, Dron dron)
        {
            sistema.cearDisenioDron(nombre, dron);
           
        }

        public void asignarComponentes(Pieza p, String nombre) 
        {           
            sistema.asignarComponentes(p,nombre);
        }

        public List<DisenioDron> getListaDisenioDrones()
        {
            return sistema.listaDisenioDrones;
        }
        #endregion

        #region CARGAR_DATOS

        public bool cargarDatos()
        {
            return sistema.cargarDatos();
        }

        #endregion

        #region MiExcepcion
        public class MiExcepcion : System.Exception
        {
            String msg;
            public MiExcepcion(String reason)
                : base(reason)
            {
                msg = reason;
            }

            public override string Message
            {
                get
                {
                    string msg = base.Message;
                    return msg;
                }
            }
        }
        #endregion
    }
}
