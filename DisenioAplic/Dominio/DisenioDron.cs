﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio
{
    public class DisenioDron
    {
        private String Nombre;
        private int Version;
        private Dron Dron;
        private List<Pieza> ListaPiezas;

        public DisenioDron(String Nombre, Dron Dron) {
            this.Dron = Dron;
            this.ListaPiezas = new List<Pieza>();
            this.Nombre = Nombre;
            this.Version = 1;
        
        }

        public String nombre
        {
            get { return Nombre; }
            set { Nombre = value; }
        }

        public int version
        {
            get { return Version; }
            set { Version = value; }
        }

        public Dron dron
        {
            get { return Dron; }
            set { Dron = value; }
        }

        public List<Pieza> listaPiezas
        {
            get { return ListaPiezas; }
            set { ListaPiezas = value; }
        }

        public override string ToString()
        {
            return nombre + " - " + version + ".0" + "Piezas: " + this.ListaPiezas.Count;
        }
    }
}
