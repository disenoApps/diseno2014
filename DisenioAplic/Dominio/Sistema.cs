﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio
{
    public class Sistema
    {
        private List<Dron> ListaDrones;
        private List<DisenioDron> ListaDisenioDrones;
        private List<Pieza> ListaPiezas;

        private static bool datosCargados;


        #region SINGLETON

         private static Sistema sistema = null;

         public static Sistema getSistema()
         {
             if (sistema == null)
             {
                 sistema = new Sistema();
             }
             return sistema;
         }

        private Sistema()
        {
            this.ListaDrones = new List<Dron>();
            this.ListaDisenioDrones = new List<DisenioDron>();
            this.ListaPiezas = new List<Pieza>();

            datosCargados = false;
        }
        #endregion

        #region GETSLISTAS
  
        public List<Dron> listaDrones
        {
            get { return ListaDrones; }
        }

        public List<DisenioDron> listaDisenioDrones
        {
            get { return ListaDisenioDrones; }
        }

        public List<Pieza> listaPiezas
        {
            get { return ListaPiezas; }
        }

        #endregion;

        #region DRON
        public void cearDron(int Id, String color, String modeloCR, float precio)
        {
            Dron nuevoDron = new Dron (Id, color, modeloCR, precio);
            this.listaDrones.Add(nuevoDron);
        }
        #endregion

        #region DISENIODRON
        public void cearDisenioDron(String nombre, Dron dron)
        {
            if (!existeDisenioDron(nombre))
            {
                DisenioDron nuevoDisenioDron = new DisenioDron(nombre, dron);
                this.listaDisenioDrones.Add(nuevoDisenioDron);
            }
            else
            {
                foreach (DisenioDron disenio in listaDisenioDrones)
                {
                    if (disenio.nombre == nombre)
                    {
                        disenio.version++;
                    }
                }
            }
        }

        public void asignarComponentes(Pieza p, String nombre)
        {
            foreach (DisenioDron disenio in listaDisenioDrones)
            {
                if (disenio.nombre == nombre)
                {
                    disenio.listaPiezas.Add(p);
                }
            }
        }


        #endregion

        #region PIEZA
        public void cearPieza(String marca, String modelo, int precision, int max, int min)
        {
            Pieza nuevaPieza= new Pieza(marca, modelo, precision, max, min);
            this.listaPiezas.Add(nuevaPieza);
        }
        #endregion

        #region VALIDACIONES
        public bool existePieza(String marca, String modelo)
        {
            foreach (Pieza pieza in listaPiezas)
            {
                if (pieza.marca == marca && pieza.modelo == modelo)
                    return true;
            }
            return false;
        }

        public bool existeDron(int id)
        {
            foreach (Dron dron in listaDrones)
            {
                if (dron.id == id)
                    return true;
            }
            return false;
        }

        public bool existeDisenioDron(String nombre)
        {
            foreach (DisenioDron disenio in listaDisenioDrones)
            {
                if (disenio.nombre == nombre)
                    return true;
            }
            return false;
        }
        #endregion

        #region CARGARDATOS
        public bool cargarDatos()
        {
            if (datosCargados)
            {
                return false;
            }
            else
            {
               Pieza pieza1 = new Pieza("MarcaBarometro1", "Barómetro", 10, 30, 20);
               listaPiezas.Add(pieza1);
               Pieza pieza2 = new Pieza("MarcaBarometro2", "Barómetro", 15, 35, 20);
               listaPiezas.Add(pieza2);
               Pieza pieza3 = new Pieza("MarcaTermometro1", "Termómetro", 100, 150, -20);
               listaPiezas.Add(pieza3);
               Pieza pieza4 = new Pieza("MarcaTermometro2", "Termómetro", 100, 100, -20);
               listaPiezas.Add(pieza4);

                datosCargados = true;
                return true;
            }
        }



        #endregion

   

    }
}
